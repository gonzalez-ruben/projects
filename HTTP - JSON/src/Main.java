import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Main {
    public static void main(String[] args) throws IOException {
        int port = 8001;
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new Server.ResponseHandler());
        server.setExecutor(null);
        server.start();

        Person jsonToPerson = JSON.JSONToPerson(HTTP.getHttpContent("http://localhost:" + port +"/"));
        System.out.println(jsonToPerson);

    }
}

