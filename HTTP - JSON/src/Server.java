import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class Server {
    public static class ResponseHandler implements HttpHandler{
        public void handle(HttpExchange exchange) throws IOException {
            Person person = new Person();
            person.setName("Ruben");
            person.setZipcode(77777);

            String json = JSON.personToJSON(person);
            exchange.sendResponseHeaders(200, json.length());
            OutputStream output = exchange.getResponseBody();
            output.write(json.getBytes());
            output.close();
        }
    }
}
