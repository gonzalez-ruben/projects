public class Games {
    public String console;
    public String title;
    public boolean played;

    public Games(String console, String title, boolean played){
        this.console = console;
        this.title = title;
        this.played = played;
    }

    @Override
    public String toString() {
        return "Title: " + title + ". Console: " + console + ". Played? " + String.valueOf(played) + ".";
    }
}
