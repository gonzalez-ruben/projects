import java.util.*;

public class Main {
    public static void main(String[] args) {
        try {
            System.out.println("\nList:");
            List newList = new ArrayList();
            newList.add("Eggs");
            newList.add("Bread");
            newList.add("Butter");
            newList.add("Chocolate Abuelita");

            for (Object str : newList) {
                System.out.println((String) str);
            }

            System.out.println("\nQueue:");
            Queue q = new PriorityQueue();
            q.add("Dishes");
            q.add("Dinner");
            q.add("Groceries");
            q.add("Bathroom");
            Iterator i = q.iterator();
            while (i.hasNext()) {
                System.out.println(q.poll());
            }

            System.out.println("\nSet:");
            Set set = new TreeSet();
            set.add("Processor");
            set.add("RAM");
            set.add("Fans");
            set.add("Battery");
            set.add("Display");
            set.add("Power");
            for (Object str : set) {
                System.out.println((String) str);
            }
            System.out.println("\nGenerics");
            List<Games> games = new LinkedList<Games>();
            games.add((new Games("Switch", "Breath of the Wild", true)));
            games.add((new Games("PS4", "Spiderman", false)));
            games.add((new Games("PC", "SMITE", true)));
            for (Games gam : games) {
                System.out.println(gam);
            }
            System.out.println("\nCompare:");
            Collections.sort(games, new Sortbyname());
            System.out.println("Sorted by Title Name");
            for (int r = 0; r < games.size(); r++) {
                System.out.println(games.get(r));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
class Sortbyname implements Comparator<Games>{

    @Override
    public int compare(Games a, Games b) {
        return a.title.compareTo(b.title);
    }
}
