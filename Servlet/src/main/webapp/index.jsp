<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome!</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<h1><%= "Hello! Please enter your details:" %>
</h1>
<br/>
<form action="servlet" method="post">
<p>First Name: <input name ="firstName" type="text" /></p>
<p>Last Name: <input name ="lastName" type="text" /></p>
<p> <input type="submit" value="Generate" /></p>
</form>
</body>
</html>