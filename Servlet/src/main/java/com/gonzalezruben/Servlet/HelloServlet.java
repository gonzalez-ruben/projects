package com.gonzalezruben.Servlet;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "Servlet", value = "/servlet")
public class HelloServlet extends HttpServlet {
        public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        try {
            if (firstName.isEmpty() | lastName.isEmpty()) {
                out.println("<html><head><title>Error!</title></head><body><h1>Text fields empty.</h1><p><a href=\"index.jsp\">Please click here to try again.</a></p> </body></html>");
            }
            else if (firstName.matches("[\\W]*") | lastName.matches("[\\W]")){
                out.println("<html><head><title>Error!</title></head><body><h1>Invalid input. Special characters are not allowed. </h1><p><a href=\"index.jsp\">Please click here to try again.</a></p> </body></html>");
            }
            else {
                out.println("<html><head><title>Welcome " + firstName + " " + lastName + "!</title><link rel=\"stylesheet\" href=\"style2.css\"></head><body>");
                out.println("<h1>Hello!</h1> <hr><h2>My Name Is</h2>");
                out.println("<h3>" + firstName + " " + lastName + "</h3>");
                out.println("<p>Pleasure to meet you</p></body></html>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + "Please use the web form to get the right content." + "</h1>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}