<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Warehouse Inventory Tracker</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: #4287f5">
        <div>
            <a href="https://www.gonzalezruben.com" class="navbar-brand"> Warehouse Inventory Tracker </a>
        </div>

        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/list"
                   class="nav-link">Products</a></li>
        </ul>
    </nav>
</header>
<br>

<div class="row">
    <div class="container">
        <h3 class="text-center">List of Products</h3>
        <c:if test="${contentMismatch == true}">
            <h4>Special Characters Not Supported!</h4>
        </c:if>
        <hr>
        <div class="container text-left">

            <a href="<%=request.getContextPath()%>/new" class="btn btn-success">Add New Product</a>
        </div>
        <br>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Item ID</th>
                <th>Name</th>
                <th>Location</th>
                <th>Stock</th>
                <th>Price</th>
                <th>Available Actions</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="product" items="${listProducts}">

                <tr>
                    <td><c:out value="${product.id}" /></td>
                    <td><c:out value="${product.name}" /></td>
                    <td><c:out value="${product.location}" /></td>
                    <td><c:out value="${product.stock}" /></td>
                    <td>$<c:out value="${product.price}" /></td>
                    <td><a href="edit?id=<c:out value='${product.id}' />">Edit</a>
                        <a
                                href="remove?id=<c:out value='${product.id}' />">Remove</a></td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </div>
</div>
</body>
</html>