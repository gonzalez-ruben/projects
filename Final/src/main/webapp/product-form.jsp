<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Warehouse Inventory Tracker</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>

<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: #4287f5">
        <div>
            <a href="https://www.gonzalezruben.com" class="navbar-brand"> Warehouse Inventory Tracker </a>
        </div>

        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/list"
                   class="nav-link">Products</a></li>
        </ul>
    </nav>
</header>
<br>
<div class="container col-md-5">
    <div class="card">
        <div class="card-body">
            <c:if test="${product != null}">
            <form action="update" method="post">
                </c:if>
                <c:if test="${product == null}">
                <form action="add" method="post">
                    </c:if>

                    <caption>
                        <h2>
                            <c:if test="${product != null}">
                                Edit Product

                            </c:if>
                            <c:if test="${product == null}">
                                Add New Product

                            </c:if>
                        </h2>
                        <c:if test="${contentMismatch == true}">
                            <h4>Special Characters Not Supported!</h4>
                        </c:if>
                        <p>All Fields Required</p>
                    </caption>

                    <c:if test="${product != null}">
                        <input type="hidden" name="id" value="<c:out value='${product.id}' />" />
                    </c:if>

                    <fieldset class="form-group">
                        <label>Item Name</label> <input type="text"
                                                        value="<c:out value='${product.name}' />" class="form-control"
                                                        name="name" required="required">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Location in Warehouse</label> <input type="text"
                                                         value="<c:out value='${product.location}' />" class="form-control"
                                                         name="location" required="required">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Current Stock</label> <input type="text"
                                                           value="<c:out value='${product.stock}' />" class="form-control"
                                                           name="stock" required="required">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Sale Price</label> <input type="text"
                                                           value="<c:out value='${product.price}' />" class="form-control"
                                                           name="price" required="required">
                    </fieldset>

                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="submit" class = "btn btn-danger" onclick="history.back();" value="Cancel">Cancel</button>

                </form>
        </div>
    </div>
</div>
</body>
</html>