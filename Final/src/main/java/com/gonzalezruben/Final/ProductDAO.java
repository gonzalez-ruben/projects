package com.gonzalezruben.Final;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.sql.PreparedStatement;
import java.util.List;

public class ProductDAO {
    SessionFactory factory = null;
    Session session = null;

    private static ProductDAO single_instance = null;

    ProductDAO(){
        factory = Utils.getSessionFactory();
    }

    public static ProductDAO getInstance(){
        single_instance = new ProductDAO();

        return single_instance;
    }
    public Product getProduct(int id){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Product where id=" + Integer.toString(id);
            Product p = (Product)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return p;
        }catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }
    public List<Product> getAllProducts(){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String sql ="from Product";
            List<Product> ps = (List<Product>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return ps;
        } catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    public Product saveProduct(String name, String location, int stock, double price){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            Product p = new Product();
            p.setName(name);
            p.setLocation(location);
            p.setStock(stock);
            p.setPrice(price);
            session.save(p);
            session.getTransaction().commit();
            return p;
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    public Product removeProduct(int id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            Product p = new Product();
            p.setId(id);
            session.delete(p);
            session.getTransaction().commit();
            return p;
        } catch (HibernateException e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    public Product updateProduct(int id, String name, String location, int stock, double price){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            Product p = new Product();
            p.setId(id);
            p.setName(name);
            p.setLocation(location);
            p.setStock(stock);
            p.setPrice(price);
            session.update(p);
            session.getTransaction().commit();
            return p;
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
}
