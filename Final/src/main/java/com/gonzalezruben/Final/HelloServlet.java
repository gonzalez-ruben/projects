package com.gonzalezruben.Final;

import java.io.*;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "Servlet", value = "/")
public class HelloServlet extends HttpServlet {
    private ProductDAO productDAO;
    public void init(){
        productDAO = new ProductDAO();
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getServletPath();

        try{
            switch(action){
                case "/new":
                    addNewProductForm(request, response);
                    break;
                case "/add":
                    addProduct(request, response);
                    break;
                case "/remove":
                    removeProduct(request, response);
                    break;
                case "/edit":
                    editProduct(request, response);
                    break;
                case "/update":
                    updateProduct(request, response);
                    break;
                default:
                    listProducts(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private void addNewProductForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("product-form.jsp");
        dispatcher.forward(request, response);
    }

    private void addProduct(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String location = request.getParameter("location");
        String stock = request.getParameter("stock");
        String price = request.getParameter("price");
        if (name.matches("[\\W]*") | location.matches("[\\W]*") | stock.matches("[\\W]*") | price.matches("[\\W]*")){
            response.sendRedirect("list");
        }
        else {
            productDAO.saveProduct(name, location, Integer.parseInt(stock), Double.parseDouble(price));
            response.sendRedirect("list");
        }
    }

    private void removeProduct(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        productDAO.removeProduct(id);
        response.sendRedirect("list");

    }

    private void editProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Product existingProduct = productDAO.getProduct(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("product-form.jsp");
        request.setAttribute("product", existingProduct);
        dispatcher.forward(request, response);
    }

    private void updateProduct(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String location = request.getParameter("location");
        String stock = request.getParameter("stock");
        String price = request.getParameter("price");
        if (name.matches("[\\W]*") | location.matches("[\\W]*") | stock.matches("[\\W]*") | price.matches("[\\W]*")){
            response.sendRedirect("list");
        } else {
            productDAO.updateProduct(Integer.parseInt(id), name, location, Integer.parseInt(stock), Double.parseDouble(price));
            response.sendRedirect("list");
        }
    }

    private void listProducts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        ProductDAO p = ProductDAO.getInstance();
        List<Product> listProducts = p.getAllProducts();
        request.setAttribute("listProducts", listProducts);
        RequestDispatcher dispatcher = request.getRequestDispatcher("product-list.jsp");
        dispatcher.forward(request, response);
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}