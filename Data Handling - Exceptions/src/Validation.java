import java.util.*;

public class Validation {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter first number:");
        float num1 = input.nextFloat();
        System.out.println("Enter second number:");
        float num2 = input.nextFloat();

        if (num2 <= 0) {
            do {
                System.out.println("Denominator cannot be zero. Please try again.");
                System.out.println("Enter second number:");
                num2 = input.nextFloat();
            } while (num2 <= 0);
        }
        calculate(num1, num2);
        input.close();

    }

    private static void calculate(float num1, float num2) throws ArithmeticException{
        float ans = num1/num2;
        System.out.println(num1 + " divided by " + num2 + " equals " + ans + ".");
    }
}
