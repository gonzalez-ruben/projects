import java.util.*;

public class Exceptions {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter first number:");
        float num1 = input.nextInt();
        System.out.println("Enter second number:");
        float num2 = input.nextInt();
        while (true){
            try{
                calculate(num1,num2);
                break;
            } catch (ArithmeticException e){
                System.out.println("Denominator cannot be zero. Please try again.");
                System.out.println("Enter second number:");
                num2 = input.nextInt();
            }
        }
        input.close();

    }

    private static void calculate(float num1, float num2) throws ArithmeticException {
        if (num2 == 0){
            throw new ArithmeticException();
        } else {
            float ans = num1/num2;
            System.out.println(num1 + " divided by " + num2 + " equals " + ans + ".");
        }
    }
}
