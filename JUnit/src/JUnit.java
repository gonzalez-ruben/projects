public class JUnit {

    public static String[] foodArray(){
        return new String[]{"bread", "jicama", "tajin"};
    }

    public static String getMovie() {
        return "Monster Hunter";
    }
    public static boolean isItFalse(){
        return false;
    }
    public static String notNull(){
        return "Correct. Not null";
    }
    public static String notTheSame(){
        return "Are these things the same?";
    }
    public static String shouldBeNull(){
        return null;
    }
    public static String theseAreSame(){
        return "Yes.";
    }
    public static boolean isItTrue(){
        return true;
    }
}
