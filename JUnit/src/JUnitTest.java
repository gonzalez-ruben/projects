import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class JUnitTest {

    @Test
    void assertArrayEqualsTest() {
        String[] currentFood = JUnit.foodArray();
        String[] expectedFood = {"bread", "jicama", "tajin"};
        assertArrayEquals(currentFood, expectedFood);
    }

    @Test
    void assertEqualsTest() {
        String currentMovie = JUnit.getMovie();
        String expectedMovie = "Monster Hunter";
        assertEquals(currentMovie, expectedMovie);
    }

    @Test
    void assertFalseTest() {
        boolean isItFalse = JUnit.isItFalse();
        assertFalse(isItFalse);
    }

    @Test
    void assertNotNullTest() {
        String notNull = JUnit.notNull();
        assertNotNull(notNull);
    }

    @Test
    void assertNotSameTest() {
        String notSameOne = JUnit.notTheSame();
        String notSameTwo = "Are they?";
        assertNotSame(notSameOne, notSameTwo);
    }

    @Test
    void assertNullTest() {
        String nullTest = JUnit.shouldBeNull();
        assertNull(nullTest);
    }

    @Test
    void assertSameTest() {
        String assertSameOne = JUnit.theseAreSame();
        String assertSameTwo = "Yes.";
        assertSame(assertSameOne, assertSameTwo);
    }

    @Test
    void assertTrueTest() {
        boolean assertTrue = JUnit.isItTrue();
        assertTrue(assertTrue);
    }
}