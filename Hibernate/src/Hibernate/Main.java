package Hibernate;

import java.util.List;

public class Main {
    public static void main(String[] args) {


        ProductDAO p = ProductDAO.getInstance();
        p.saveProduct("Cinnamon Rolls", "Breakfast Isle", 13);

        List<Product> prod = p.getProducts();
        for(Product i : prod){
            System.out.println(i);
        }
        System.out.println(p.getProduct(2));

    }
}
