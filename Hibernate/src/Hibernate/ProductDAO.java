package Hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;
public class ProductDAO {
    SessionFactory factory = null;
    Session session = null;

    private static ProductDAO single_instance = null;

    ProductDAO(){
        factory = HibernateUtils.getSessionFactory();
    }
    public static ProductDAO getInstance(){
        if(single_instance == null){
            single_instance = new ProductDAO();
        }
        return single_instance;
    }
    public List<Product> getProducts(){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String sql ="from Product";
            List<Product> ps = (List<Product>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return ps;
        } catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    public Product getProduct(int id){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Product where id=" + Integer.toString(id);
            Product p = (Product)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return p;
        }catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }
    public Product saveProduct(String name, String location, int instock){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            Product p = new Product();
            p.setName(name);
            p.setLocation(location);
            p.setInstock(instock);
            session.save(p);
            session.getTransaction().commit();
            return p;
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
}
