import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(3);

        Threads t1 = new Threads("3060 Ti", 45, 120);
        Threads t2 = new Threads("2070 Super", 28, 140);
        Threads t3 = new Threads("3070", 62, 131);
        Threads t4 = new Threads("3090", 119, 350);
        Threads t5 = new Threads("5700 XT", 49.5, 140);
        Threads t6 = new Threads("3080", 96, 240);


        service.execute(t1);
        service.execute(t2);
        service.execute(t3);
        service.execute(t4);
        service.execute(t5);
        service.execute(t6);

        service.shutdown();

    }
}
