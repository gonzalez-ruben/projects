import java.util.Random;

public class Threads implements Runnable {
    private String cardName;
    private double hashRate;
    private int speed;
    private int target;

    public Threads(String cardName, double hashRate, int speed){
        this.cardName = cardName;
        this.hashRate = hashRate;
        this.speed = speed;

        Random random = new Random();
        this.target = random.nextInt(5000 + 1) + 1000;
    }
    @Override
    public void run() {
        System.out.println("Executing: " + cardName + ". Hash Rate: " + hashRate + " Mh/s. Updating Speed: " + speed + ". Target: " + target +"\n");
        if (cardName.isEmpty() || hashRate <=0){
            System.out.println("This Java application requires that a Card name be set and a Hash Rate of more than 0 for the application to run properly." +
                    "\nPlease fix these parameters before continuing." +
                    "\n\nThis card has been skipped.");
        } else {
            for (int count = 1; count < target; count++) {
                if (count % hashRate == 0) {
                    System.out.println(cardName + " Processing...");
                    try {
                        Thread.sleep(speed);
                    } catch (InterruptedException e) {
                        System.out.println("Something went wrong. Please double check that all parameters are entered correctly.");
                        e.printStackTrace();
                    }
                }
            }
            System.out.println(cardName + " is done!\n");
        }
    }
}
